<?php
/**
 * Created by PhpStorm.
 * User: prabhjyot
 * Date: 19/01/16
 * Time: 7:09 PM
 */

namespace com\teamoxio\oxio_dao;




class Security
{
    public static function encode($decoded)
    {
        $mine=array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','!','#','$','%','&','(',')','*','+',',','-','.','/');
        $hash=$decoded;
        $arr=array();
        $count=strlen($hash);
        $string='';
        for($i=0; $i<$count; $i++)
        {
            $rand=rand(11,99);
            $ord=(ord($hash[$i])*$rand);
            $hexa=(hexdec($ord)*$rand);
            $arr[]=$hexa.$rand;
        }
        $arrCount=count($arr);
        $mineCount=count($mine);
        for($i=0;$i<$arrCount;$i++)
        {
            $rand=rand(0,$mineCount-1);
            $string.=$arr[$i].$mine[$rand];
        }
        return $string;
    }

    public static function decode($encoded){
        $string=$encoded;
        preg_match_all("/([0-9]+)/", $string,$data);
        $decode="";
        for($i=0;$i<count($data[0]); $i++)
        {
            $str=$data[0][$i];
            $count=strlen($str);
            $rand='';
            $string='';
            $rand=$str[$count-2].$str[$count-1];
            for($j=0;$j<=$count-3; $j++)
            {
                $string.=$str[$j];
            }
            $rand=(int)$rand;
            $string=(int)$string;

            $hexa=$string/$rand;
            $ord=dechex($hexa)/$rand;
            $decode.=chr($ord);
        }
        return $decode;
    }



}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rohit Oberoi
 * Date: 15/1/16
 * Time: 7:19 PM
 * To change this template use File | Settings | File Templates.
 */

namespace com\teamoxio\oxio_dao;


class Init {
    public static $OxioDAO=null;

    public static function register($oxio_dao){
        self::$OxioDAO = $oxio_dao;
    }

    public static function validate(){

        if(self::$OxioDAO==null)
            die("Init Error");
        if(self::$OxioDAO->config['db_name']=="")
            die("Enter db name");
    }

    public static function connectDB(){
        $db = new \PDO('mysql:host='.self::$OxioDAO->config['db_host'].';dbname='.self::$OxioDAO->config['db_name'].';charset=utf8', self::$OxioDAO->config['db_username'], self::$OxioDAO->config['db_password']);

//        $response = \mysql_connect(self::$OxioDAO->config['db_host'],
//            self::$OxioDAO->config['db_username'],
//            self::$OxioDAO->config['db_password']
//        );
//        if(!$response){
//            throw new \Exception("MySQL Error: Error in connecting to database");
//        }
//        $response = \mysql_select_db(self::$OxioDAO->config['db_name']);
//        if(!$response){
//            throw new \Exception("MySQL Error: Error in selecting database");
//        }

        Database::$db_name = self::$OxioDAO->config['db_name'];
        Database::$db = $db;
        Database::registerAllTables();
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: prabhjyot
 * Date: 28/01/16
 * Time: 4:50 PM
 */

namespace com\teamoxio\oxio_dao;


class ValidatorMessages
{
    const REQUIRED_MESSAGE = "{field} is a required field.";
    const MAX_LENGTH_MESSAGE = "{field} has a maximum length of {max}";
    const MIN_LENGTH_MESSAGE = "{field} has a minimum length of {min}";

    const TYPE_REQUIRED = 1;
    const TYPE_MAX_LENGTH = 2;
    const TYPE_MIN_LENGTH = 3;

    public static function generateMessage($message,$field,$type){

        switch($type){
            case self::TYPE_REQUIRED:
                $message = str_replace("{field}",$field,$message);
                break;
            case self::TYPE_MAX_LENGTH:
                $message = str_replace("{field}",$field->name,$message);
                $message = str_replace("{max}",$field->length,$message);
                break;
            case self::TYPE_MIN_LENGTH:
                break;
        }

        return $message;
    }
}
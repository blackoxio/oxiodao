<?php
/**
 * Created by PhpStorm.
 * User: prabhjyot
 * Date: 25/01/16
 * Time: 4:37 PM
 */

namespace com\teamoxio\oxio_dao;


class StdClass
{
    public function __set($property,$value){
        $this->$property = $value;
    }
    public function __get($property){
        if(isset($this->$property)){
            return $property;
        }
        else{
            return "";
        }
    }
}
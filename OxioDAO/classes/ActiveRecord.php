<?php
/**
 * Created by PhpStorm.
 * User: prabhjyot
 * Date: 19/01/16
 * Time: 7:09 PM
 */

namespace com\teamoxio\oxio_dao;


class ActiveRecord
{

    protected $table_name;
    public $old_condition=null;

    public function __construct($table_name){
        $this->table_name = $table_name;
    }
    public function __set($property,$value){
        $this->$property = $value;
    }
    public function __get($property){
        if(isset($this->$property)){
           return $property;
        }
        else{
            return "";
        }
    }
    public function save(){

        return $this->update(get_object_vars($this));
    }

    private function update($columns){
        //get primary column
        $primary_column = Database::getPrimaryColumn($this->table_name);
//        if(!isset($this->$primary_column))
//        {
//            throw new \Exception("ActiveRecord: You cannot update a table which doesn't has a primary index on an integer column");
//        }
        $query = "UPDATE `".$this->table_name."` SET ";
        $cols = "";

        $values = array();
        foreach($columns as $column=>$value){
            if($column=="table_name"|| $column==$primary_column || $column=="old_condition")
                continue;

            $cols.= " `".$column."` = ?, ";
            $values[] = $value;


        }
        $cols = rtrim($cols,", ");

        if(!isset($this->$primary_column)){
            $query .= $cols ." WHERE ".$this->old_condition;
        }
        else if($this->old_condition!=null)
            $query .= $cols ." WHERE ".$primary_column." = ".$this->$primary_column;
        else
            throw new \Exception("ActiveRecord: You cannot update this table.");


        //check rules
        if($this->validate($columns)) {

            return Database::insert($query, $values);
        }
        else{
//            print_r($this->errors);
            return false;
        }

    }
    public function validate($params){
        $validator= Validator::init($this->table_name,$params);
        if($validator->validate())
        {
            return true;
        }
        else {
            $this->errors = $validator->getErrors();
            return false;
        }
    }

    public function getErrors($field=null){
        if($field == null)
            return $this->errors;
        else if(isset($this->errors[$field]))
            return $this->errors[$field];
        else
            return null;
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: prabhjyot
 * Date: 19/01/16
 * Time: 7:09 PM
 */

namespace com\teamoxio\oxio_dao;




class Database
{
    const COLUMN_TYPE_INT = "int";

    public static $db_name = null;
    public static $db= null;

    public static function sanitize($variable){
//        return mysql_real_escape_string($variable);
//        $variable = str_replace("''","''",$variable);
        $variable = self::$db->quote($variable);
        return $variable;
    }

    public static function getPrimaryValue(){
        return self::$db->lastInsertId();
    }
    public static function getTableSchema($table_name){
        return json_decode(file_get_contents(\OxioDAO::$ROOT_PATH."schemas/".$table_name.".schema.oxio"));
    }
    public static function getPrimaryColumn($table_name){
        $schema = self::getTableSchema($table_name);
        $columns = $schema->columns;
        $primary_column = null;
        foreach($columns as $column){
            if(isset($column->primary) && $column->primary == 1 && $column->type == self::COLUMN_TYPE_INT){
                $primary_column = $column->name;
            }
        }

        return $primary_column;
    }


    public static function insert($statement,$params){
        $statement = self::$db->prepare($statement);
        $result = $statement->execute($params);
        if($result< 1)
            return false;
        return $result;
    }
    public static function count($sql){
        try{
            $result = self::$db->query($sql);
        }
        catch(Exception $e){
            throw new \Exception("Database Error: " . $e->getMessage());
        }

        return (int)$result->rowCount();
//        $result = \mysql_query($sql);
//        if(!$result){
//            throw new \Exception("Mysql Error: " . mysql_error());
//        }
//        return \mysql_num_rows($result);
    }

    public static function queryAll($sql,$table_name=false){
//        $result = \mysql_query($sql);
        try{
           // echo $sql."<br />";
            $result = self::$db->query($sql);
        }
        catch(Exception $e){
            throw new \Exception("Database Error: " . $e->getMessage());
        }
//        if(!$result){
//            throw new \Exception("Mysql Error: " . mysql_error());
//        }
        $result = $result->fetchAll(\PDO::FETCH_ASSOC);

        $response = array();
        if($result ==null)
            return $response;
//        if(mysql_num_rows($result)<1){
//            return $response;
//        }
//        while ($row = mysql_fetch_object($result)) {
        foreach($result as $row){
            if($table_name!==false)
            $object = new ActiveRecord($table_name);
            else
            $object = new StdClass();

            $old_condition = "";

            foreach($row as $prop=>$val){
                $object->$prop = $val;
                $old_condition.= " `".$prop."` = '".$val."' AND ";

            }
            $old_condition = rtrim($old_condition,"AND ");
            $object->old_condition = $old_condition;

            $response[] = $object;
        }

        return $response;


    }
    public static function query($sql,$table_name){

        try {
            $result = self::$db->query($sql);
        }
        catch(Exception $e){

            throw new \Exception("Database Error: " . $e->getMessage());
        }

//        $result = \mysql_query($sql);

        if($result == null){
            throw new \Exception("Database Error: Error in executing SQL query.");
        }

        $result = $result->fetch(\PDO::FETCH_ASSOC);
        if($result==null){
            //throw new \Exception("Database Error: " . $e->getMessage());
            return null;
        }

        if($table_name!==false)
            $object = new ActiveRecord($table_name);
        else
            $object = new StdClass();
//        if(mysql_num_rows($result)<1){
//            return null;
//        }
//        $row = mysql_fetch_assoc($result);

        $old_condition = "";
        foreach($result as $prop=>$val){
            $object->$prop = $val;
            $old_condition.= " `".$prop."` = '".$val."' AND ";
        }
        $old_condition = rtrim($old_condition,"AND ");
        $object->old_condition = $old_condition;
        return $object;

    }

    public static function registerAllTables(){
        $query="SHOW TABLES";
        $tables=self::queryAll($query);
        $temp = "Tables_in_".self::$db_name;
        foreach($tables as $table)
        {
            \OxioDAO::registerTable($table->$temp);
        }
    }


    public static function getTableStructure($table_name){
        $query = "DESCRIBE ".$table_name;
        $columns = self::queryAll($query);


        $schema = array();
        foreach($columns as $column){

            $primary = false;
            if($column->Extra == "auto_increment" && $column->Key=="PRI"){
                $primary = true;
            }
            $type = false;
            $length =false;
            $required = true;
            $default = false;
            $null  = false;

            //check if column is optional
            if($column->Default!=null){
                $default = $column->Default;
            }

            if(strtolower($column->Null) != "no" ){
                $null = true;
            }

            if($null === true || $default !== false || $primary===true)
                $required=false;


            //get length
            preg_match_all('/(\w+)\(\d+\)/iU',$column->Type,$matches);

            if(isset($matches[0][0])){
                preg_match_all('/\(\d+\)/iU',$column->Type,$matches1);
                $length = str_replace(array("(",")"),"",$matches1[0][0]);
            }

            if(isset($matches[1][0])){
                $type = trim($matches[1][0]);
            }
            else{
                $type = $column->Type;
            }




            if($type===false)
            {

                throw new \Exception("Generation Error: Error in registering table ".$table_name." for column: ".$column->Field);
            }

            $temp_column = array(
                "name"=>$column->Field,
                "type"=>$type,
                "length"=>$length,
            );

            if($primary)
                $temp_column['primary'] = $primary;
            if($default!==false)
                $temp_column['default'] = $default;
            if($null)
                $temp_column['null'] = $null;
            if($required) {
                $temp_column['required'] = $required;
                $schema['required'][] = $column->Field;
            }

            $schema["columns"][] = $temp_column;


        }

        return $schema;
    }


}
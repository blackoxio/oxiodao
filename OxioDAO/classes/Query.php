<?php
/**
 * Created by PhpStorm.
 * User: prabhjyot
 * Date: 21/01/16
 * Time: 6:05 PM
 */

//namespace com\teamoxio\oxio_dao;

use \com\teamoxio\oxio_dao\Database;
use \com\teamoxio\oxio_dao\Validator;

class Query
{
    protected $where = array();
    protected $orWhere = array();
    protected $andWhere = array();
    protected $limit ="";
    protected $offset ="";
    protected $order ="";
    protected $group =array();


    protected $table_name = null;
    protected $query="";

    protected $errors = array();

    protected $isNewRecord = false;
    protected $insertProperties = array();

    public function save(){
        //attempt to insert
        $columns = $this->insertProperties;

        return $this->insert($columns);
    }

    public function __set($prop,$value){
        if($this->isNewRecord){
            $this->insertProperties[$prop]=$value;
        }
        $this->$prop = $value;
    }

    public function __construct($table_name,$isNewRecord=true){
      $this->table_name = $table_name;
        $this->isNewRecord = $isNewRecord;
        if($this->table_name == "" || $this->table_name==null){
            throw new \Exception("Query: Unable to initialize query. Table name missing.");
        }
    }
    public static function on($table_name){
        return new Query($table_name,false);
    }

    public function where($condition){
        array_push($this->where,$condition);
        return $this;
    }
    public function orWhere($condition){
        array_push($this->orWhere,$condition);
        return $this;
    }
    public function andWhere($condition){
        array_push($this->andWhere,$condition);
        return $this;
    }
    public function order($order){
        $this->order = $order;
        return $this;
    }
    public function group($group){
        $this->group = $group;
        return $this;
    }

    public function limit($limit){
        $this->limit = $limit;
        return $this;
    }
    public function offset($offset){
        $this->offset = $offset;
        return $this;
    }

    public function beforeExecute()
    {
        $this->query = $this->generateQuery();
    }
    public function all(){
        $this->beforeExecute();
        $result = Database::queryAll($this->query,$this->table_name);
        return $result;
    }
    public function one(){
        $this->limit = " 1 ";
        $this->beforeExecute();
        $result = Database::query($this->query,$this->table_name);
        return $result;
    }
    public function count(){
        $this->beforeExecute();
        $result = Database::count($this->query);
        //return (int)count($result);
        return $result;
    }


    private function insert($columns){
        $query = "INSERT INTO `".$this->table_name."` ";
        $cols = $values = "";
        $params = array();
        foreach($columns as $column=>$value){
            $cols.= " `".$column."`, ";
            $values.=":$column, ";
            $params[$column] =$value;
        }
        $cols = rtrim($cols,", ");
        $values = rtrim($values,", ");

        $query .= " ($cols) VALUES ($values) ";

        //check rules
        if($this->validate($params)) {

            if(Database::insert($query, $params)){
                //setup primary column
                $primary = Database::getPrimaryColumn($this->table_name);
                $value = Database::getPrimaryValue();
                $this->$primary = $value;
                return true;
            }
            else{
                return false;
            }
        }
        else{
//            print_r($this->errors);
            return false;
        }

    }
    public function validate($params){
        $validator= Validator::init($this->table_name,$params);
        if($validator->validate())
        {
            return true;
        }
        else {
            $this->errors = $validator->getErrors();
            return false;
        }
    }

    public function generateQuery(){
        $query = "SELECT * FROM `".$this->table_name."`";

//        $params = array();
//        $paramIndex = ":p";

        $sub_condition ="";
        foreach($this->where as $where){
            if(is_array($where)){
                foreach($where as $column=>$value){
                    $sub_condition.=" `".$column."` = '".$value."' AND ";
                }

            }
            else{
                $sub_condition.=" ".$where." AND ";
            }
        }
        //check for AND
        $sub_condition = rtrim($sub_condition,"AND ");

        foreach($this->andWhere as $where){
            if(is_array($where)){
                foreach($where as $column=>$value){
                    $sub_condition.=" AND `".$column."` = '".$value."' ";
                }

            }
            else{
                $sub_condition.=" AND ".$where." ";
            }
        }

        foreach($this->orWhere as $where){
            if(is_array($where)){
                foreach($where as $column=>$value){
                    $sub_condition.=" OR `".$column."` = '".$value."' ";
                }

            }
            else{
                $sub_condition.=" OR ".$where." ";
            }
        }

        //check for AND
        $sub_condition = rtrim($sub_condition,"AND ");
        if($sub_condition!="")
        $sql = $query. " WHERE ".$sub_condition;
        else
        $sql = $query;

        //check for group
//        if(count($this->group)>0){
        if(is_array($this->group)){
            if(count($this->group)>0) {
                //add group clause
                if (is_array($this->group)) {
                    $sql .= " GROUP BY ";
                    foreach ($this->group as $column) {
                        $sql .= $column. ", ";
                    }
                    $sql = rtrim($sql, ", ");
                }
            }
        }
        else{
            if($this->group!="")
                $sql .= " GROUP BY " . $this->group . " ";
        }

        //check for order
        if($this->order!=""){
            //add order clause
            $sql .= " ORDER BY ".$this->order." ";
        }

        //limit/offset phase
        if($this->offset!="" && $this->limit=="")
        {
            throw new \Exception("Query Error: Limit cannot be blank if offset is given.");
        }
        if($this->limit !="" && $this->offset!=""){
            $sql.= " LIMIT ".$this->offset.",".$this->limit;
        }
        else if($this->limit!=""){
            $sql .= " LIMIT ".$this->limit;
        }


        return $sql;
    }
    public function getErrors($field=null){
        if($field == null)
            return $this->errors;
        else if(isset($this->errors[$field]))
            return $this->errors[$field];
        else
            return null;
    }
}
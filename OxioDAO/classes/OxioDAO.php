<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rohit Oberoi
 * Date: 15/1/16
 * Time: 6:33 PM
 * To change this template use File | Settings | File Templates.
 */

use com\teamoxio\oxio_dao\Init;
use com\teamoxio\oxio_dao\Database;
use com\teamoxio\oxio_dao\Helper;


class OxioDAO {
    public $config = array();
    protected static $instance = null;
    public $defaults = array(
        "db_type"=>"mysql",
        "db_host"=>"localhost",
        "db_username"=>"root",
        "db_name"=>"",
        "db_password"=>""
    );

    protected static $generate_schemas = false;

    public static $database=null;

    public static $ROOT_PATH ;

    public function __construct($config){
        //$this->config= $config;
        $this->config = array_merge($this->defaults,$config);
        self::$instance = $this;

    }
   public static function init($config = array())
    {
        self::$ROOT_PATH =  dirname(__FILE__)."/../";
        return new OxioDAO($config);

    }
    public function run(){
        Init::register(self::$instance);
        Init::validate();
        Init::connectDB();
    }

    public function getConfig(){
        return $this->defaults;
    }

    public static function regenerateSchemas(){
        //FOR NOW DISABLED DUE TO CONCERNS
        return;
        self::$generate_schemas = true;
    }


    public static function registerTable($table_name){
        if(self::$database == null){
            self::$database = new Database();

        }
        if(!file_exists(self::$ROOT_PATH."schemas/".$table_name.".schema.oxio") || self::$generate_schemas!==false) {
            //add it to tables list and generate schema
            $columns = self::$database->getTableStructure($table_name);

            //$schema = serialize($columns);
            $schema=json_encode($columns);
            $schema=Helper::prettyPrint($schema);
            file_put_contents(self::$ROOT_PATH."schemas/" . $table_name . ".schema.oxio", $schema);
            //print_r($columns);
        }

    }

}
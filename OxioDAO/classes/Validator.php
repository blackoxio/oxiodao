<?php
/**
 * Created by PhpStorm.
 * User: prabhjyot
 * Date: 28/01/16
 * Time: 4:43 PM
 */

namespace com\teamoxio\oxio_dao;


class Validator
{
    protected $params = array();
    protected $table_name = null;
    protected $schema = null;
    protected $errors = array();

    public function getErrors($field=null){
        if($field == null)
            return $this->errors;
        else if(isset($this->errors[$field]))
            return $this->errors[$field];
        else
            return null;
    }
    public static function init($table_name,$params){

        return new Validator($table_name,$params);
    }

    public function __construct($table_name,$params){
        //get schema
        $schema = Database::getTableSchema($table_name);
        $this->table_name = $table_name;
        $this->schema = $schema;
        $this->params = $params;

        if($this->schema==null || $this->table_name == null ){
            throw new \Exception("Validator: Error in initializing validator engine");
        }
    }
    public function addError($field,$message){
        if(isset($this->errors[$field])){

            array_push($this->errors[$field],$message);
        }
        else{
            $this->errors[$field] = array($message);

        }
    }

    public function validate(){
        $this->checkRequired();
        $this->checkLength();
        $this->checkDataTypes();

        if(count($this->errors)<1)
            return true;
        else{

            return false;
        }
    }

    public function checkRequired(){
        //get required columns
        $required = $this->schema->required;
        foreach($required as $column){
          if(!isset($this->params[$column]) || $this->params[$column]=="" || $this->params[$column]==null ){
              $this->addError($column,ValidatorMessages::generateMessage(ValidatorMessages::REQUIRED_MESSAGE,$column,ValidatorMessages::TYPE_REQUIRED));
          }
        }
    }

    public function checkLength(){
        //get all columns
        $columns = $this->schema->columns;
        foreach($columns as $column){
            if($column->length===false)
                continue;
            if(!isset($this->params[$column->name])){
                continue;
            }
            if( strlen($this->params[$column->name]) > $column->length)
                $this->addError($column->name,ValidatorMessages::generateMessage(ValidatorMessages::MAX_LENGTH_MESSAGE,$column,ValidatorMessages::TYPE_MAX_LENGTH));
//            if($this->params[$column]=="" || $this->params[$column]==null ){
//                $this->addError($column,ValidatorMessages::generateMessage(ValidatorMessages::REQUIRED_MESSAGE,$column,ValidatorMessages::TYPE_REQUIRED));
//            }
        }

    }

    public function checkDataTypes(){

    }

}